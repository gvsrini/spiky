import gulp from 'gulp';
import path from 'path';
import karma from 'karma';

// Run the headless unit tests as you make changes.
gulp.task('watch:phantom', done => {
	process.env.NODE_ENV = 'test';

	let watchStarted = false;
	let server = new karma.Server({
		configFile: path.resolve('config/karma.conf.babel.js'),
		browsers: ['PhantomJS'],
		singleRun: false
	});
	server.on('run_complete', () => {
		if (!watchStarted) {
			watchStarted = true;
			done();
		}
	});
	server.start();
});