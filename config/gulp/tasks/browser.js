import gulp  from 'gulp';
import livereload from 'gulp-livereload';
import fs from 'fs';
import glob from 'glob';
import path from 'path';
import mkdirp from 'mkdirp';
import { rollup } from 'rollup';
import json from 'rollup-plugin-json';
import commonjs from 'rollup-plugin-commonjs';
import babel from 'rollup-plugin-babel';
import nodeResolve from 'rollup-plugin-node-resolve';
import multiEntry from 'rollup-plugin-multi-entry';
import typeScript from 'rollup-plugin-typescript';
import pkg from '../../../package.json';
import capitalizeFirstLetter from '../util/capitalizeFirstLetter';

let firstBuild = true;

// Set up a livereload environment for our spec runner `test/runner.html`
gulp.task('browser', ['lint', 'clean:tmp'], done => {

	const testFiles = glob.sync('./test/browser-tests/**/*browser.js')
		.concat(glob.sync('./test/node-tests/**/*node.js'));
	rollup({
		entry: ['./config/setup/browser.js'].concat(testFiles),
		plugins: [
			nodeResolve({
				jsnext: true,
				main: true
			}),
			json(),
			typeScript(),
			babel({
				babelrc: false,
				sourceMaps: true,
				presets: ['es2015-rollup']
			}),
			commonjs(),
			multiEntry.default()
		]
	}).then(bundle => {
		const result = bundle.generate({
			format: 'umd',
			moduleName: capitalizeFirstLetter(pkg.name),
			sourceMap: 'inline',
			sourceMapSource: 'index.js',
			sourceMapFile: pkg.name + '.js'
		});

		// Write the generated sourcemap
		mkdirp.sync('tmp');
		fs.writeFileSync(path.join('tmp', '__specs.js'), result.code + '\n//# sourceMappingURL=./__specs.js.map');
		fs.writeFileSync(path.join('tmp', '__specs.js.map'), result.map.toString());

		if (firstBuild) {
			// we listen to both the browser and node.js unit tests
			livereload.listen({port: 35729, host: 'localhost', start: true});
			gulp.watch(['src/**/*', 'test/browser-tests/**/*browser.js', 'test/node-tests/**/*node.js', 'package.json'], ['browser']);
		} else {
			livereload.reload('./tmp/__specs.js');
		}
		firstBuild = false;

		done();
	}).catch(console.error);
});

