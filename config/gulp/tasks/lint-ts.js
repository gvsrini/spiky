import gulp from 'gulp';
import tslint from 'tslint';
import customRules from '../util/customRules';

// Lint the TypeScript files
gulp.task('lint:ts', () =>
	gulp.src('src/**/*.ts')
		.pipe(tslint({
			rulesDirectory: customRules()
		}))
		.pipe(tslint.report('stylish', {
			emitError: false,
			sort: true,
			bell: true,
			fullPath: true
		}))
);